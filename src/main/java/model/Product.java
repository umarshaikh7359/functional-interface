package model;

import java.util.Set;

public class Product
{

	private Long id;

	private String name;

	private String category;

	private Double price;

	private Set<Order> orders;

	public Product(final String name, final String category, final Double price)
	{
		this.name = name;
		this.category = category;
		this.price = price;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(final Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(final String name)
	{
		this.name = name;
	}

	public String getCategory()
	{
		return category;
	}

	public void setCategory(final String category)
	{
		this.category = category;
	}

	public Double getPrice()
	{
		return price;
	}

	public void setPrice(final Double price)
	{
		this.price = price;
	}

	public Set<Order> getOrders()
	{
		return orders;
	}

	public void setOrders(final Set<Order> orders)
	{
		this.orders = orders;
	}

	@Override
	public String toString()
	{
		return "Product{" +
				"\nid=" + id +
				", \nname='" + name + '\'' +
				", \ncategory='" + category + '\'' +
				", \nprice=" + price +
				", \norders=" + orders +
				'}';
	}
}
