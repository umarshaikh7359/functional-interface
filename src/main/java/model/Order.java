package model;

import java.time.LocalDate;
import java.util.Set;

public class Order
{

	private Long id;

	private LocalDate orderDate;

	private LocalDate deliveryDate;
	
	private String status;

	private Customer customer;

	Set<Product> products;


	public Order(final Long id, final LocalDate orderDate, final LocalDate deliveryDate, final String status, final Customer customer, final Set<Product> products)
	{
		this.id = id;
		this.orderDate = orderDate;
		this.deliveryDate = deliveryDate;
		this.status = status;
		this.customer = customer;
		this.products = products;
	}

	public Order()
	{
	}

	public Long getId()
	{
		return id;
	}

	public void setId(final Long id)
	{
		this.id = id;
	}

	public LocalDate getOrderDate()
	{
		return orderDate;
	}

	public void setOrderDate(final LocalDate orderDate)
	{
		this.orderDate = orderDate;
	}

	public LocalDate getDeliveryDate()
	{
		return deliveryDate;
	}

	public void setDeliveryDate(final LocalDate deliveryDate)
	{
		this.deliveryDate = deliveryDate;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(final String status)
	{
		this.status = status;
	}

	public Customer getCustomer()
	{
		return customer;
	}

	public void setCustomer(final Customer customer)
	{
		this.customer = customer;
	}

	public Set<Product> getProducts()
	{
		return products;
	}

	public void setProducts(final Set<Product> products)
	{
		this.products = products;
	}
}
