package model;


public class Customer
{
	private Long id;
	
	private String name;
	
	private Integer tier;

	public Customer(final Long id, final String name, final Integer tier)
	{
		this.id = id;
		this.name = name;
		this.tier = tier;
	}

	public Customer()
	{
	}

	public Long getId()
	{
		return id;
	}

	public void setId(final Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(final String name)
	{
		this.name = name;
	}

	public Integer getTier()
	{
		return tier;
	}

	public void setTier(final Integer tier)
	{
		this.tier = tier;
	}
}
