
import model.Product;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.UnaryOperator;

public class Main
{
	static Product product = new Product("Realme-GT","Phone",18999.0);


	public static void main(String[] args)
	{
		predicateExample();

		biPredicateExample();

		functionalExample();

		biFunctionalExample();

		consumerExample();

		biConsumerExample();

		unaryOperatorExample();

		binaryOperatorExample();

		toIntFunctionExample();

		toDoubleFunctionExample();
	}


	static void predicateExample() {
		Predicate<Integer> isNumberZero = (a) -> a == 0;
		Predicate<Integer> isNumberOne = (a) -> a == 1;

		System.out.println(isNumberZero.test(10)); // test
		System.out.println(isNumberZero.and(isNumberOne).test(1)); // and
		System.out.println(isNumberZero.negate().test(1));         // negate
		System.out.println(isNumberZero.or(isNumberOne).test(1));  // or
	}

	static void biPredicateExample() {
		BiPredicate<Integer, String> isNumberInString = (integer, s) -> s.contains(integer.toString());
		System.out.println(isNumberInString.test(10, "umar10"));
	}

	static void functionalExample() {
		Function<Double, Double> doSqure = (a) -> a*a;
		System.out.println(doSqure.apply(2.0));
	}

	static void biFunctionalExample() {
		BiFunction<Double, Double, String> doPlus = (a, b) -> a+"+"+b+"="+(a+b);
		System.out.println(doPlus.apply(1.2, 12.4));
	}

	static void consumerExample() {
		Consumer<List<Integer>> printEven = (a) -> {
			a.forEach(integer -> {
				if (integer%2 == 0) {
					System.out.print(integer+" ");
				}
			});
			System.out.println();
		};
		List<Integer> integers = List.of(1,2,3,4,5,6);
		printEven.accept(integers);
	}

	static void biConsumerExample() {
		BiConsumer<String, String> printFullName = (firstName, lastname) -> System.out.println(firstName+" "+lastname);
		printFullName.accept("Umar", "Shaikh");
	}

	static void unaryOperatorExample() {
		UnaryOperator<Integer> twosMultiple = (a) -> a*2;
		System.out.println(twosMultiple.apply(123));
	}

	static void binaryOperatorExample() {
		BinaryOperator<String> concat = (s, s2) -> s+" "+s2;
		System.out.println(concat.apply("Hello", "World"));

		BinaryOperator<Integer> op = BinaryOperator.maxBy(Integer::compareTo);
		System.out.println(op.apply(98, 11));

		BinaryOperator<Integer> op2 = BinaryOperator.minBy(Integer::compareTo);
		System.out.println(op2.apply(98, 11));
	}

	static void toIntFunctionExample() {
		ToIntFunction<Product> getProductPrice = (product1) -> product1.getPrice().intValue();
		System.out.println("Product's price is: "+ getProductPrice.applyAsInt(product));
	}

	static void toDoubleFunctionExample() {
		ToDoubleFunction<Product> apply10PercentDiscount = (product1) -> product1.getPrice() - (product1.getPrice()*10)/100;
		System.out.println(apply10PercentDiscount.applyAsDouble(product));
	}
}